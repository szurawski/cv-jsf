package pl.codementors;

import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Service class for deep cloning of objects. Used in this example to simulate persistence storage. Objects returned
 * from storage are cloned. Thanks to that changes made by set methods do not update date in storage.
 */
public class Cloner {

    private static final Logger log = Logger.getLogger(Cloner.class.getName());

    /**
     * Performs deep object cloning. Returned object and whole its content have new references.
     *
     * @param object Object to be cloned. Must be serializable.
     * @param <T> Object type.
     * @return Cloned object with new reference.
     */
    public static <T> T clone(T object) {
        byte[] bytes = writeObject(object);
        if (bytes != null) {
            return (T) readObject(bytes);
        }
        return null;
    }

    /**
     * Deserialized object from byte array.
     *
     * @param bytes Serialized object as byte array.
     * @return Deserialized object.
     */
    private static Object readObject(byte[] bytes) {
        try (ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
             ObjectInputStream ois = new ObjectInputStream(bais)) {
            return ois.readObject();
        } catch (IOException | ClassNotFoundException ex) {
            log.log(Level.WARNING, ex.getMessage(), ex);
            return null;
        }
    }

    /**
     * Saves serializable object to byte array.
     *
     * @param object Object to be serialized.
     * @return Serialized object as byte array.
     */
    private static byte[] writeObject(Object object) {
        try (ByteArrayOutputStream baos = new ByteArrayOutputStream();
             ObjectOutputStream oos = new ObjectOutputStream(baos)) {
            oos.writeObject(object);
            return baos.toByteArray();
        } catch (IOException ex) {
            log.log(Level.WARNING, ex.getMessage(), ex);
            return null;
        }
    }

}
