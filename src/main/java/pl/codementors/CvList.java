package pl.codementors;


import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

@Named
@ViewScoped
public class CvList implements Serializable {

    @Inject
    private CvDataStore store;

    private Cv cvs;


    public Cv getCvs(){
        if (cvs == null) {
            cvs = store.getCV();
        }
        return cvs;
    }

}
