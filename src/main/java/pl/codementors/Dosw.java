package pl.codementors;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "doswiadczenie")
public class Dosw implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    private String okresPracy;

    @Column
    private String firma;

    @Column
    private String stanowisko;

    public Dosw() {
    }

    @ManyToOne
    private Cv cv;

    public Dosw(String okresPracy, String firma, String stanowisko) {
        this.okresPracy = okresPracy;
        this.firma = firma;
        this.stanowisko = stanowisko;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOkresPracy() {
        return okresPracy;
    }

    public void setOkresPracy(String okresPracy) {
        this.okresPracy = okresPracy;
    }

    public String getFirma() {
        return firma;
    }

    public void setFirma(String firma) {
        this.firma = firma;
    }

    public String getStanowisko() {
        return stanowisko;
    }

    public void setStanowisko(String stanowisko) {
        this.stanowisko = stanowisko;
    }
}
